function [He,Hg,Hgpronly,gfac,smooth_fac,glac_fac,bound_fac,mi] = glate_inv(DEM,TMP,CST,DEB,xgpr,ygpr,Hgpr,Hgprtop,Hgprbot,gfac,Hg,AMB)

% Input parameters:
% DEM:          Digital elevation model (GRIDobj)
% TMP:          Glacier mask (0: outside of glacier, 1: on glacier)
% CST:          Constants and control parameters
% DEB:          Mask with debris cover
% xgpr, ygpr:   x and y coordinates of GPR points
% Hgpr:         Ice thickness at GPR points
% Hgprtop:      Minimum ice thickness at GPR points according to GPR uncertainty
% Hgprbot:      Maximum ice thickness at GPR points according to GPR uncertainty
% gfac:         calibration factor for unconstrained glaciological model
%               (to be computed, when gfac <= 0)
% GFAC          Grid with Flowshed depenedent calibration factors
% Hg:           Unconstrained model (to be computed, when Hg = []). Hg will be
%               used for glate inversion in the form of "gfac*Hg"
% AMB:          Measured ambient mass balance values (to be used, when AMB ~= [])
%
% Output parameters
% He:           GlaTE output model
% Hg:           Unconstrained glaciological model
% Hpgronly:     Interpolated model from GPR data only
% gfac:         calibration factor for unconstrained glaciological model
% smooth_fac:   Final smoothing factor
% glac_fac:     Final weighting factor for glaciological constraints
% bound_fac:    Final boundaty weighting factor
% mi:           Percentage of fitted GPR data (0 <= mi <= 1)

if (nargin ~= 12)
    error('Input error glate_inv! Usage: \n[He,Hg,Hgpronly,gfac,smooth_fac,glac_fac,bound_fac,mi] = glate_inv(DEM,TMP,CST,DEB,xgpr,ygpr,Hgpr,Hgprtop,Hgprbot,gfac,Hg,AMB)')
end

%%
% Determine grid coordinates
xy1 = [1,1,1]*DEM.refmat;
xy2 = [size(DEM.Z),1]*DEM.refmat;
DEMX = xy1(1):DEM.refmat(2,1):xy2(1);
DEMY = xy1(2):DEM.refmat(1,2):xy2(2);
nx = DEM.size(2);
ny = DEM.size(1);
nxy = nx * ny;
[yy,xx] = ndgrid(DEMY,DEMX);

% Make sure that the DEM does not include nan values
ii = find(isnan(DEM.Z));
ii2 = find(~isnan(DEM.Z));
if (isempty(ii) == 0)
    F = scatteredInterpolant(xx(ii2),yy(ii2),DEM.Z(ii2),'nearest','nearest');
    DEM.Z(ii) = F(xx(ii),yy(ii));
end

% Make sure that the boundary of the model does not include glacier
TMP.Z(1,:) = nan;
TMP.Z(end,:) = nan;
TMP.Z(:,1) = nan;
TMP.Z(:,end) = nan;

SRF.iig = find(~isnan(TMP.Z)); % areas on glacier
SRF.iing = find(isnan(TMP.Z)); % areas outside of glacier

% Some consistency checks
% Glacier mask should not extend to the boundary of DEM
if ((length(DEM.Z(1,:)) ~= length(TMP.Z(1,:))) || ...
        (length(DEM.Z(:,1)) ~= length(TMP.Z(:,1))))
    error('Dimension mis-match of DEM and glacier mask')
end
if ((sum(isnan(TMP.Z(1,:))) ~= DEM.size(2)) | ...
        (sum(isnan(TMP.Z(DEM.size(1),:))) ~= DEM.size(2)) | ...
        (sum(isnan(TMP.Z(:,1))) ~= DEM.size(1)) | ...
        (sum(isnan(TMP.Z(:,DEM.size(2)))) ~= DEM.size(1)))
    error('Glacier area extends to DEM boundary - increase DEM')
end

% Determine gpridx
ngpr = length(Hgpr);
if (ngpr > 0)
    xidx = zeros(ngpr,1);
    xdist = zeros(ngpr,1);
    yidx = zeros(ngpr,1);
    ydist = zeros(ngpr,1);
    for a = 1:ngpr, [xdist(a),xidx(a)] = min(abs(DEMX-xgpr(a))); end
    for a = 1:ngpr, [ydist(a),yidx(a)] = min(abs(DEMY-ygpr(a))); end
    gpridx = sub2ind(size(DEM.Z),yidx,xidx);
    di = sqrt(xdist.^2 + ydist.^2);
    di = di + 1.0e-8;
    
    % Exclude GPR points outside of glacier mask
    ii = find(ismember(gpridx,SRF.iing) == 0);
    gpridx = gpridx(ii);
    xgpr = xgpr(ii);
    ygpr = ygpr(ii);
    Hgpr = Hgpr(ii);
    Hgprtop = Hgprtop(ii);
    Hgprbot = Hgprbot(ii);
    
    di = di(ii);
    ngpr = length(ii);
    
    % Interpolate/Extrapolate GPR data on nearest grid point
    Hgpronly = nan * ones(size(DEM.Z));
    Hgprtoponly = nan * ones(size(DEM.Z));
    Hgprbotonly = nan * ones(size(DEM.Z));
    use = zeros(ngpr,1);
    for a = 1:ngpr
        if (use(a) == 1), continue; end
        ii = find(gpridx == gpridx(a));
        Hgpronly(gpridx(a)) = sum(1./di(ii).*Hgpr(ii))/sum(1./di(ii));
        Hgprtoponly(gpridx(a)) = sum(1./di(ii).*Hgprtop(ii))/sum(1./di(ii));
        Hgprbotonly(gpridx(a)) = sum(1./di(ii).*Hgprbot(ii))/sum(1./di(ii));
    end
    
    % Remove gpridx entries, where Hgpronly(gpridx) <= 0)
    tmp = Hgpronly(gpridx);
    ii = find(tmp > 0);
    gpridx = gpridx(ii);
    ngpr = length(ii);
    
    
    % make sure top thickness limit extends not below zero thickness
    i1 = find(Hgprtoponly(gpridx) <= 0);
    if sum(i1) ~= 0
        fprintf('Warning: %i/%i cases, after interpolating for modeling grid, top thickness limit extends not below zero thickness. Setting corresponding limit thickness to zero\n',length(i1),length(gpridx))
        Hgprtoponly(gpridx(i1)) = 0;
    end
    
    % check whether bottom thickness limit extends  above Hgpronly
    
    i1 = find(sum(Hgprbotonly(gpridx) <= Hgpronly(gpridx)));
    if sum(i1) ~= 0
        fprintf('Warning: %i/%i cases, bottom thickness limit extends above Hgpronly. Setting them 5%% below Hgpronly',length(i1),length(gpridx))
        Hgprbotonly(gpridx(i1)) = Hgpronly(gpridx(i1))-Hgpronly(gpridx(i1))*0.05;
    end
end

% Compute and restrict slope angles and
% compute surface gradient term in equation (4) of Clarke et al
% Remove spikes in surface gradient term and apply gentle smoothing
% (spikes may appear at glacier boundaries)
GMP = gradient8(DEM,'rad');
SRF.SA = GMP.Z;
SRF.SA(SRF.SA > CST.stmax) = CST.stmax;
SRF.SA(SRF.SA < CST.stmin) = CST.stmin;
SRF.SRFG = (1.0 + tan(SRF.SA).^2)./abs(tan(SRF.SA));
SRF.SRFG = medfilt2(SRF.SRFG,[CST.smooth_SA CST.smooth_SA]);

%% Uncalibrated glate run for determining Hg
if (isempty(Hg) == 1)
    Hg = glate(DEM,SRF,CST,AMB,DEB);
    if (CST.apriori_smoothing > 0)
        Hg = smooth2a(Hg,CST.apriori_smoothing,CST.apriori_smoothing);
    end
end
Hg(isnan(Hg)) = 0; % Set nan values to zero

if (ngpr == 0) % no GPR data
    He = Hg*gfac;
    nit = -1;
    He(SRF.iing) = NaN;
    Hg(SRF.iing) = NaN;
    smooth_fac = NaN;
    glac_fac = NaN;
    bound_fac = NaN;
    mi = NaN;
    return;
end

if gfac == -1
    % Determine gfac for entire glacier
    gfac = Hg(gpridx)\Hgpronly(gpridx);
    He = [];
    nit = [];
    Hg(SRF.iing) = NaN;
    smooth_fac = NaN;
    glac_fac = NaN;
    bound_fac = NaN;
    mi = NaN;
    return;
end

% Determine gfac for entire glacier
gfac = Hg(gpridx)\Hgpronly(gpridx);

%% Setup inversion components

% Estimate ice tickness gradient in the vicinity of the glacier boundaries
nb = CST.nb;
if nb ~= 0
    % determine boundary index:
    TMP2 = TMP;
    for i1 = 1:nb+1
        GRAD = TMP2;
        if i1 == 1 % i1 = 1: boundary cells outside glacier
            GRAD.Z(~isnan(GRAD.Z)) = 0;
            GRAD.Z(isnan(GRAD.Z)) = 1;
        else % i1 > 1: Cells inside boundary
            GRAD.Z(~isnan(GRAD.Z)) = 1;
            GRAD.Z(isnan(GRAD.Z)) = 0;
        end
        GRAD = gradient8(GRAD);
        bd(i1).idx = find(GRAD.Z > 0)';
        TMP2.Z(bd(i1).idx) = nan;
    end
    bdidx = [bd(1:end-1).idx]; % Index of cells for boundary gradients
    bdidx2 = bd(end).idx;      % Index of cells clarke-thickness constrain
    
    % Gradient of the DEM
    Z = medfilt2(DEM.Z,[5 5],'symmetric');
    [sx,sy] = gradient(Z,1,1);
    sx0 = medfilt2(flipud(sx),[5 5],'symmetric');
    sy0 = medfilt2(flipud(sy),[5 5],'symmetric');
    
    % Extrapolate gradients from region outside glacier to region inside glacier:
    sx1 = nan(size(sx0));
    sy1 = sx1;
    iing = setdiff(SRF.iing,bd(1).idx);
    iig = [SRF.iig;bd(1).idx'];
    F = scatteredInterpolant(xx(iing),yy(iing),sx(iing),'natural','none');
    sx1(iig) = F(xx(iig),yy(iig));
    F = scatteredInterpolant(xx(iing),yy(iing),sy(iing),'natural','none');
    sy1(iig) = F(xx(iig),yy(iig));
    
    % Thickness gradients (difference from glacier surface gradients and
    % extrapolated gradients):
    sx2 = sx0-sx1;
    sy2 = sy0-sy1;
    
    sx3 = zeros(size(sx2)); sx3(bdidx) = sx2(bdidx);
    sy3 = zeros(size(sy2)); sy3(bdidx) = sy2(bdidx);
    stot = sx3+sy3;
    tgrd = reshape((stot),nxy,1)*1.6; % factor 1.6 due to differences of gradient() and LD*H
else
    bdidx = [];
    bdidx2 = [];
    tgrd = [];
end

% Setup index array excluding boundary points
idx = 1:nxy;
idx = reshape(idx,ny,nx);
idx(1,:) = 0;
idx(ny,:) = 0;
idx(:,1) = 0;
idx(:,nx) = 0;
idx = reshape(idx,nxy,1);
idx = idx(idx > 0);
nidx = length(idx);
oidx = ones(1,nidx);

% Setup smoothing operator LS
ii = [idx             idx              idx             idx             idx];
jj = [idx             idx+1            idx-1           idx+ny          idx-ny];
val = [oidx       -0.25*oidx -0.25*oidx -0.25*oidx -0.25*oidx];
LS = sparse(ii,jj,val,nxy,nxy);
RhsLS = zeros(nxy,1);

% Setup difference operator LD for Clarke model gradients
idx2 = setdiff(idx,bdidx);
nidx = length(idx2);
oidx2 = ones(1,nidx);
ii = [];
jj = [];
val = [];

% Horizontal and vertical differences
ii = [ii idx2        idx2        idx2       idx2];
jj = [jj idx2+1    idx2-1    idx2+ny  idx2-ny];
val = [val oidx2/2.0 -oidx2/2.0 oidx2/2.0 -oidx2/2.0];

% Diagonal elements
ii = [ii   idx2                  idx2                idx2                idx2];
jj = [jj   idx2+ny+1           idx2-ny-1         idx2+ny-1         idx2-ny+1];
val = [val oidx2/sqrt(8)    -oidx2/sqrt(8) oidx2/sqrt(8)  -oidx2/sqrt(8)];

% Add 8 further near-diagonal differences to the LD
% since two differences have always the same angle, give them only 50% weight
ii = [ii idx2 idx2]; jj = [jj idx2+ny+ 1 idx2-ny+ 0]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
ii = [ii idx2 idx2]; jj = [jj idx2+ny- 1 idx2-ny+ 0]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
ii = [ii idx2 idx2]; jj = [jj idx2+ny- 1 idx2- 0+ 1]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
ii = [ii idx2 idx2]; jj = [jj idx2-ny- 1 idx2- 0+ 1]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
ii = [ii idx2 idx2]; jj = [jj idx2-ny+ 1 idx2+ny+ 0]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
ii = [ii idx2 idx2]; jj = [jj idx2-ny- 1 idx2+ny+ 0]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
ii = [ii idx2 idx2]; jj = [jj idx2+ny+ 1 idx2+ 0- 1]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
ii = [ii idx2 idx2]; jj = [jj idx2-ny+ 1 idx2+ 0- 1]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];

% Compute difference operator LD and right hand side of difference operator
LD = sparse(ii,jj,val,nxy,nxy);
RhsLD = LD*reshape(Hg*gfac,nxy,1);

% Setup difference operator LB for boundary gradients
if nb > 0
    idx2 = bdidx;
    nidx = length(idx2);
    oidx2 = ones(1,nidx);
    ii = [];
    jj = [];
    val = [];
    
    % Horizontal and vertical differences
    ii = [ii idx2        idx2        idx2       idx2];
    jj = [jj idx2+1    idx2-1    idx2+ny  idx2-ny];
    val = [val oidx2/2.0 -oidx2/2.0 oidx2/2.0 -oidx2/2.0];
    
    % Diagonal elements
    ii = [ii   idx2                  idx2                idx2                idx2];
    jj = [jj   idx2+ny+1           idx2-ny-1         idx2+ny-1         idx2-ny+1];
    val = [val oidx2/sqrt(8)    -oidx2/sqrt(8) oidx2/sqrt(8)  -oidx2/sqrt(8)];
    
    % Add 8 further near-diagonal differences to the LD
    % since two differences have always the same angle, give them only 50% weight
    ii = [ii idx2 idx2]; jj = [jj idx2+ny+ 1 idx2-ny+ 0]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
    ii = [ii idx2 idx2]; jj = [jj idx2+ny- 1 idx2-ny+ 0]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
    ii = [ii idx2 idx2]; jj = [jj idx2+ny- 1 idx2- 0+ 1]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
    ii = [ii idx2 idx2]; jj = [jj idx2-ny- 1 idx2- 0+ 1]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
    ii = [ii idx2 idx2]; jj = [jj idx2-ny+ 1 idx2+ny+ 0]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
    ii = [ii idx2 idx2]; jj = [jj idx2-ny- 1 idx2+ny+ 0]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
    ii = [ii idx2 idx2]; jj = [jj idx2+ny+ 1 idx2+ 0- 1]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
    ii = [ii idx2 idx2]; jj = [jj idx2-ny+ 1 idx2+ 0- 1]; val = [val 0.5*oidx2/sqrt(5) -0.5*oidx2/sqrt(5)];
    
    % Compute difference operator BD ...
    BD = sparse(ii,jj,val,nxy,nxy);
    
    % ... and right hand side of difference operator
    RhsBD0 = BD*reshape(Hg*gfac,nxy,1);
    RhsBD  = reshape(tgrd,nxy,1);
    
    % Keep Clarke-gradients in case sign is changing
    grdrat = RhsBD0./(RhsBD);
    RhsBD(grdrat<0) = RhsBD0(grdrat<0);
    
    % only allow limited deviation from original Clarke gradients
    grdfac = CST.grdfac;
    grdrat = RhsBD0./(RhsBD);
    RhsBD(grdrat<1/grdfac) = RhsBD0(grdrat<1/grdfac)*grdfac;
    RhsBD(grdrat>  grdfac) = RhsBD0(grdrat>  grdfac)/grdfac;
    
    % Setup constraints for Clarke-model-boundary
    ncb = length(bdidx2);
    ii = 1:ncb;
    jj = bdidx2';
    LhsCb = sparse(ii,jj,ones(size(ii)),ncb,nxy);
    RhsCb = Hg(bdidx2)'*gfac;
else
    BD = [];
    RhsBD = [];
    LhsCb = [];
    RhsCb = [];
end

% Setup constraints for areas outside of glacier and clarke-boundary
% contraints
nng = length(SRF.iing);
ii = 1:(nng);
jj = SRF.iing;
LhsNg = sparse(ii,jj,ones(size(ii)),nng,nxy);
RhsNg = zeros(nng,1);

% Setup GPR part of equation system
ii = 1:ngpr;
jj = gpridx;
LhsGpr = sparse(ii,jj,ones(size(ii)),ngpr,nxy);

% Make sure that Hgpronly(gpridx) is a column vector
RhsGpr = Hgpronly(gpridx);
if (length(RhsGpr(1,:)) > length(RhsGpr(:,1)))
    RhsGpr = RhsGpr';
end

%% Main inversion loops
% lambda1 = 1
glac_fac = CST.glacfac_ini; % initial lambda 2
bound_fac1 = CST.boundfac_ini; % initial lambda 3
bound_fac2 = glac_fac; if bound_fac2 < 1, bound_fac2 = 1; end 
smoothfac_min1 = CST.smoothfac_min; % initial lambda 4

nit0 = 0;
nit_prev = 0;
while(1) % Outer inversion loop over glac_fac
    nit0 = nit0 + 1;
    % Initialize regularization parameter
    smooth_fac = CST.smoothfac_ini;
    if nit_prev ~= 0
        smooth_fac = smooth_fac / CST.smooth_adjust^(nit_prev-1); % don't start from smoothfac_ini (this was tested already with lower glac_fac)
    end
    
    if CST.smoothfac_min < smoothfac_min1
        bound_fac1 = 0.75*glac_fac;
        if glac_fac > 1, bound_fac2 = glac_fac;
        else, bound_fac2 = 1;
        end
    end
    nit = 0;
    fprintf('Adapting smoothing factor (glac_fac = %.2f, bound_fac = %.2f):\n',glac_fac,bound_fac1)
    while(1) % inner inversion loop over smooth_fac
        nit = nit + 1;
        
        % Solve system of equations
        He =  [glac_fac*[LD;LhsCb];smooth_fac*LS;LhsGpr;bound_fac1*LhsNg;bound_fac2*BD] \[glac_fac*[RhsLD;RhsCb];smooth_fac*RhsLS;RhsGpr;bound_fac1*RhsNg;bound_fac2*RhsBD];
        He(He < 0) = 0;
        He(SRF.iing) = 0;
        
        % check number of bedrock points being withing uncertainty range and check exit criterion
        in = ((Hgprbotonly(gpridx) >= He(gpridx)) + (He(gpridx) >= Hgprtoponly(gpridx))) == 2;
        mi = sum(in)/ngpr;
        fprintf(' --- %.2f (mi = %.2f)\n',smooth_fac,mi)
        
        if (mi >= CST.reg_acc), break; end
        
        % Adjust smoothing factor
        if (smooth_fac / CST.smooth_adjust < CST.smoothfac_min), break; end
        smooth_fac = smooth_fac / CST.smooth_adjust;
        
    end
    fprintf(' === Inner loop with glacfac %g finished. nit: %d smooth_fac: %g  mi: %g\n',glac_fac,nit,smooth_fac,mi)
    nit_prev = nit;
    
    % Check exit criteria outer loop
    if (mi < CST.reg_acc) || (glac_fac >=  CST.glacfac_min && CST.smoothfac_min == CST.smoothfac_min2)
        if glac_fac <  CST.glacfac_min && CST.smoothfac_min > CST.smoothfac_min2 % if minimum glac_fac not reached, allow lower smoothing_fac
            CST.smoothfac_ini = mean([CST.smoothfac_ini,CST.smoothfac_min2]);
            CST.smoothfac_min = CST.smoothfac_min2;
            CST.glacfac_max = CST.glacfac_min*CST.glac_adjust;
        else % if minimum glac_fac is reached, or if lower smoothing_fac is already tested
            if nit0 > 1 && mi < mi_prev
                mi = mi_prev;
                He = He_prev;
                glac_fac = glac_fac_prev;
                smooth_fac = smooth_fac_prev;
                bound_fac1 = bound_fac_prev;
            end
            fprintf('Outer loop with glacfac %g finished. nit: %d smooth_fac: %g  mi: %g\n',glac_fac,nit0,smooth_fac,mi)
            break;
        end
    end
    
    He_prev = He;
    mi_prev = mi;
    glac_fac_prev = glac_fac;
    smooth_fac_prev = smooth_fac;
    bound_fac_prev = bound_fac1;
    
    % Adjust glac_fac
    if (glac_fac * CST.glac_adjust >= CST.glacfac_max)
        fprintf('Outer loop with glacfac %g finished. nit: %d smooth_fac: %g  mi: %g\n',glac_fac,nit,smooth_fac,mi)
        break;
    end
    glac_fac = glac_fac * CST.glac_adjust;
    
end

% Setup final output parameters
He = reshape(He,ny,nx);
He(SRF.iing) = NaN;
Hg = [];
Hgpronly(SRF.iing) = NaN;
bound_fac = bound_fac1;

