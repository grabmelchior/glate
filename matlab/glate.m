function H = glate(DEM,SRF,CST,AMB,DEB)
%
% glate performs ice thickness estimations following Clarke et al. (2013)
%
% Input parameters: 
%       DEM : DEM GRIDobj
%       SRF : structure containing surface-related information
%       CST : Constants and parameters
%       DEB : debris GRIDobj
% Output parameters: 
%       H : Matrix containing ice thickness estimates

if isempty(DEB)
    DEB = DEM;
    DEB.Z = zeros(size(DEM.Z));
    CST.fdebr = 1;
end

% Compute weighted line segments to be used for determing the flux
% (eqs 22 to 25 in Clarke et al. 2013)
dx = DEM.cellsize;
dy = DEM.cellsize;
[sx,sy] = gradient(DEM.Z,dx,dy);
wn = -sy; wn(wn < 0) = 0;
ws = sy;  ws(ws < 0) = 0;
we = -sx; we(we < 0) = 0;
ww = sx;  ww(ww < 0) = 0;
dl = (wn*dy + we*dx + ws*dy + ww*dx) ./ ...
     sqrt(wn.^2 + we.^2 + ws.^2 + ww.^2);

% Determine flowsheds
DEM2 = DEM;
DEM2.Z = nan*ones(size(DEM2.Z));
DEM2.Z(SRF.iig) = DEM.Z(SRF.iig);
FD = FLOWobj(DEM2);
DB = drainagebasins(FD);

% DB.Z(DB.Z > 0) = 1; % make a single flowshed (for testing purposes)
if (isempty(AMB) == 0)
    DB.Z(DB.Z > 0) = 1;
end
nfs = numel(unique(DB.Z(:))) - 1;

% Coordinate grid
xy1 = [1,1,1]*DEM.refmat; 
xy2 = [size(DEM.Z),1]*DEM.refmat; 
DEMX = xy1(1):DEM.refmat(2,1):xy2(1);
DEMY = xy1(2):DEM.refmat(1,2):xy2(2);
[yy,xx] = ndgrid(DEMY,DEMX);

% Loop over flowsheds
TAU = nan*ones(size(DEM.Z));    
for a = 1:nfs
    
    % Identify elements of flowshed a
    ii = DB.Z == a;
    if (isempty(ii) == 1), continue; end;

    % Compute apparent balance rate
    if (isempty(AMB) == 1)
        z = DEM.Z(ii);
        ela = mean(z);
        ela = fzero(@(ela) CompBal(ela,z,CST.gacc,CST.gabl),ela);
        z = z - ela;
        b = zeros(size(z));
        b(z > 0) = z(z > 0) * CST.gacc;
        b(z <= 0) = z(z <= 0) * CST.gabl;
        B = nan*ones(size(DEM.Z));
        B(ii) = b;
    else
        B = nan*ones(size(DEM.Z));
        B(ii) = AMB.Z(ii);
    end
    
    % Reduce apparent mass balance at debris covered areas
    DEB.Z(B > 0) = 0;                           % Ignore debris in apparent accumulation areas.
    B(DEB.Z==1) = B(DEB.Z==1)*(1 - CST.fdebr);
    
    % Compute ice flux
    bval = linspace(max(max(B)),min(min(B)),CST.nbzone);
    bzone = nan * ones(size(B));
    bsum = zeros(CST.nbzone,1);
    avsintheta = zeros(size(B));
    qa = zeros(size(B));
    for c = 1:CST.nbzone
        iib = find((B > bval(c)) & (isnan(bzone) == 1));
        iic = find(B > bval(c));
        bzone(iib) = c;
        bsum(c) = sum(B(iic)) * dx * dy;
    end
    
    % - Determine interface line between zones of constant b,
    % - Compute and assign average ice flux qa
    % - Determine average slope along interface line
    for c = 1:CST.nbzone
        bline = zeros(size(bzone));
        iic = find(bzone == c);
        iin = (bzone(iic+1) == c+1) | (isnan(bzone(iic+1) == 1));
        iis = (bzone(iic-1) == c+1) | (isnan(bzone(iic-1) == 1));
        iie = (bzone(iic+DEM.size(1)) == c+1) | (isnan(bzone(iic+DEM.size(1))) == 1);
        iiw = (bzone(iic-DEM.size(1)) == c+1) | (isnan(bzone(iic-DEM.size(1))) == 1);   
        bline(iic(iin)) = 1;
        bline(iic(iis)) = 1;
        bline(iic(iie)) = 1;
        bline(iic(iiw)) = 1;
        blinesum = sum(dl((bline == 1) & (isnan(dl) == 0)));
        if (blinesum > 0)
            qa(iic) = bsum(c)/blinesum;
        end
        
        avsintheta(iic) = mean(sin(SRF.SA((bline == 1) & (isnan(dl) == 0))));        
    end
    
    % Interpolate for NaN values in avsintheta (and in qa at the same positions)
    if sum(sum(isnan(avsintheta))) ~= 0
        i1 = find(isnan(avsintheta) == 1);
        i2 = find((isnan(avsintheta) == 0) + (avsintheta~=0) == 2);
        
        F = scatteredInterpolant(xx(i2),yy(i2),avsintheta(i2),'natural','none');
        if numel(F(xx(i1),yy(i1))) == numel(i1)
            avsintheta(i1) = F(xx(i1),yy(i1));
        else
            i3 = find(avsintheta ~= 0);
            avsintheta(i3) = fillmissing(avsintheta(i3), 'linear', 'EndValues', 'nearest');
        end
        
        F = scatteredInterpolant(xx(i2),yy(i2),qa(i2),'natural','none');
        if numel(F(xx(i1),yy(i1))) == numel(i1)
            qa(i1) = F(xx(i1),yy(i1));
        else
            i3 = find(qa ~= 0);
            qa(i3) = fillmissing(qa(i3), 'linear', 'EndValues', 'nearest');
        end
    end
    
    % Convert qa to m2 s-1 (from m2 yr-1).
    qa = qa ./ (365.25*86400);
    
    % Compute basal stress tau*
    TAU(ii) = ((CST.n+2)*(CST.rho*CST.g*avsintheta(ii)).^2 * CST.gsi .* qa(ii)/(2.0*CST.A)).^(1/(CST.n+2));
    
end

% Initial determination of H
H = SRF.SRFG .* TAU/(CST.rho*CST.g);

% longitudinal averaging after Kamb & Echelmeyer (1986)

% Coordinate grid
xy1 = [1,1,1]*DEM.refmat; 
xy2 = [size(DEM.Z),1]*DEM.refmat; 
DEMX = xy1(1):DEM.refmat(2,1):xy2(1);
DEMY = xy1(2):DEM.refmat(1,2):xy2(2);
[yy,xx] = ndgrid(DEMY,DEMX);

% Total gradient and gradient direction
sgtot = sqrt(sx.^2 + sy.^2) + 1.0e-8;
cosa = sx./sgtot; 
sina = sy./sgtot;

% Loop over cells with no-nan TAU values
ii = find(~isnan(TAU));
cosa = cosa(ii);
sina = sina(ii);
TAU_smooth = nan*ones(size(TAU));
lfac = CST.l_factor*H;
lfac = lfac(ii);
for a = 1:length(ii)
    
    % Distances
    dx = (xx-xx(ii(a)));
    dy = (yy-yy(ii(a)));
    dx = dx(ii);
    dy = dy(ii);
    dd = sqrt(dx.^2 + dy.^2);
    
    % cos squared of angle between gradient direction and direction
    % between ii(a)th cell and all other cells
    % cos(a-b) = cos(a)*cos(b) + sin(a)*sin(b)
    cosab = dx./dd .* cosa + dy./dd .* sina;
    lafunc = exp(-dd./lfac) .* cosab.^2;
    lafunc(a) = 1.0;
    TAU_smooth(ii(a)) = sum(TAU(ii).*lafunc)/sum(lafunc);
end

% Final determination of H
H = SRF.SRFG .* TAU_smooth/(CST.rho*CST.g)  ;


function val = CompBal(ela,z,gacc,gabl)
%

q = zeros(size(z));
z = z - ela;
q(z > 0) = z(z > 0) * gacc;
q(z <= 0) = z(z <= 0) * gabl;
val = sum(q);
