function glate_wrapper(tag)
%
% This is an example wrapper file that demonstrates, how to use glate_inv.m and glate.m.
%
% In comparison to the version presented by Langhammer et al. (2019), 
% this is an extended version, which additionally includes a glacier boundary 
% gradient and reduced apparent mass balances for debris covered areas.
% These additional features can be omitted by setting
% CST.nb = 0
% CST.fdebr = -1

% Use one of the following tags
% 'mort': Morteratsch
% 'plaine': Plaine Morte

% Define root directory and modify matlab path
pp = 'd:/maurer/programs/glate/GITLAB/'; % Add your location
pp = '/Users/andromeda/Documents/MATLAB/glate_GitHub/';
if (exist('glate.m','file') ~= 2)
    addpath([pp 'matlab']);
end

% Add path to topotoolbox and fix ambiguity with the resample function
topo_toolbox_path = 'd:/maurer/programs/topotoolbox-master/'; % add your location
topo_toolbox_path = '/Users/andromeda/Documents/MATLAB/GlaTE/matlab_functions/topotoolbox-master/';
if (exist('GRIDobj.m','file') ~= 2)
    addpath(genpath(topo_toolbox_path));
end
pwdd = (pwd);
cd([topo_toolbox_path '@GRIDobj']);
my_resample = @resample;
cd(pwdd);

% Constants and parameters
CST.stmin              = 5.0/180*pi;  % minimum slope angle allowed [radians]
CST.stmax              = 60.0/180*pi; % maximum slope angle allowed [radians]
CST.smooth_SA          = 5;           % smoothing of surface geometry factor [cell units]
CST.gacc               = 0.0025;      % q gradient in accumulation zone [m w.e. m-1 yr-1]
CST.gabl               = 0.0025*9/5;  % q gradient in ablation zone [m w.e. m-1 yr-1]
CST.n                  = 3;           % Exponent of Glen's law [-]
CST.A                  = 2.4e-24;     % creep factor [s-1 Pa-3]
CST.rho                = 910;         % ice density [kg m-3]
CST.g                  = 9.81;        % earth gravity acceleration [m s-2]
CST.gsi                = 0.5;         % fraction of creep relative to basal sliding [fraction]
CST.nbzone             = 20;          % number of zones of constant AMB rates within a flowshed [-]
CST.l_factor           = 2;           % exponential factor for longitudinal averaging [-]
CST.apriori_smoothing  = 0;           % smoothing of unconstrained model [cell units]
CST.target_acc         = 0.05;        % GPR accuracy, used if no min/max GPR thickness is given [%]
CST.reg_acc            = 0.95;        % required minimum GPR points modeled within top/bottom thickness [fraction]
CST.glacfac_ini        = 0.45;        % Initial weight for glaciological constraints [-]
CST.glacfac_min        = 0.75;        % Minimum value for (final) glaciological constraints [-]
CST.glacfac_max        = 3.0;         % Maximum value for (final) glaciological constraints [-]
CST.glac_adjust        = 1.1;         % Adjustment of glaciological constraints factor [fraction]
CST.smoothfac_ini      = 12.0;        % Initial smoothing factor for inversion [-]
CST.smoothfac_min      = 5.0;         % Minimum smoothing factor [-]
CST.smoothfac_min2     = 2.0;         % Minimum smoothing factor in case minimum glacfac_min can not be reached [-]
CST.smooth_adjust      = 1.1;         % Adjustment of smoothing factor [fraction]
CST.boundfac_ini       = 1;           % initial weight for boundary constraints [-]
CST.nb                 = 3;           % width of boundary region, to which boundary gradients get applied [cell units]
CST.grdfac             = 5;           % maximum gradient alteration (1/grdfac < grad(Glac)/grad(B) < grdfac) [-]
CST.fdebr              = 0.4;         % Reduction of AMB in debris-covered ablation areas [fraction], set fdebr = -1 to ignore debris cover

% Digital elevation model
if (strcmp(tag,'dom') == 1)
    DEMfile = [pp 'data/' tag '_dem.tif']; 
else
    DEMfile = [pp 'data/' tag '_dem.asc']; 
end

% Raster file with debris cover:
if CST.fdebr ~= -1
    DEBfile = [pp 'data/' tag '_debris.asc'];
end

% Mast with glacier extent:
GlacierMaskFile = [pp 'data/' tag '_mask.asc']; % Glacier mask (0: outside, 1: inside)

% Ice thickness point data:
GPRData = [pp 'data/' tag '_GPR_data.mat']; % variables xgpr ygpr hgpr pno (optional Hgprtop Hgprbot)

% Read original DEM, glacier mask, and debris mask, and prepare some grid parameters
DEM = GRIDobj(DEMfile);
DEM.Z = double(DEM.Z);
DEMX = DEM.refmat(3,1):DEM.refmat(2,1):DEM.refmat(3,1) + DEM.size(2)*DEM.refmat(2,1);
DEMY = DEM.refmat(3,2):DEM.refmat(1,2):DEM.refmat(3,2) + DEM.size(1)*DEM.refmat(1,2);
DEMX = DEMX(1:DEM.size(2)) + 0.5*DEM.refmat(2,1);
DEMY = DEMY(1:DEM.size(1)) + 0.5*DEM.refmat(1,2);
[yy,xx] = ndgrid(DEMY,DEMX);
TMP = GRIDobj(GlacierMaskFile);
if CST.fdebr ~= -1
    DEB = GRIDobj(DEBfile);
    DEB.Z = double(DEB.Z);
else
    DEB = [];
end

% Read GPR data 
load(GPRData,'xgpr','ygpr','Hgpr');
ngpr = length(xgpr); % no of GPR data points

% Determine gpridx (nearest grid point of DEM of each GPR point)
xidx = zeros(ngpr,1);
xdist = zeros(ngpr,1);
yidx = zeros(ngpr,1);
ydist = zeros(ngpr,1);
for a = 1:ngpr, [xdist(a),xidx(a)] = min(abs(DEMX-xgpr(a))); end
for a = 1:ngpr, [ydist(a),yidx(a)] = min(abs(DEMY-ygpr(a))); end
gpridx = sub2ind(size(DEM.Z),yidx,xidx);

% Exclude GPR points outside of glacier mask
SRF.iig = find(~isnan(TMP.Z)); % areas on glacier
SRF.iing = find(isnan(TMP.Z)); % areas outside of glacier
ii = find(ismember(gpridx,SRF.iing) == 0);
gpridx = gpridx(ii);
xgpr = xgpr(ii);
ygpr = ygpr(ii);
Hgpr = Hgpr(ii);

% if available, minimum/maximum ice thickness at GPR points according to 
% GPR uncertainty. Here we use a 5% GPR uncertainty: 
if ~exist('Hgprtop','var'), Hgprtop = Hgpr*(1-CST.target_acc); end
if ~exist('Hgprbot','var'), Hgprbot = Hgpr*(1+CST.target_acc); end

% perform unconstrained (no GPR data) glate inversion and determine gfac
 [~,Hgla,~,gfac,~,~,~,~] = glate_inv(DEM,TMP,CST,DEB,xgpr,ygpr,Hgpr,Hgprtop,Hgprbot,-1,[],[]);

 % Perform GlaTE inversion using gfac and the glaciological constraints
[Hall,~,Hgpronly,~,smooth_fac,glac_fac,bound_fac,mi] = glate_inv(DEM,TMP,CST,DEB,xgpr,ygpr,Hgpr,Hgprtop,Hgprbot,gfac,Hgla,[]);

% Plotting parameters
cax1 = [0 250];
cax2 = [-60 60];
clf;

% Determine glacier outline
GRAD = TMP;
GRAD.Z(isnan(GRAD.Z)) = 0;
GRAD = gradient8(GRAD);
dbx =xx(GRAD.Z > 0);
dby =yy(GRAD.Z > 0);

% Plot uncsonstrained model
plot_results(DEM,Hgla*gfac,dbx,dby,[],[],[],flipud(viridis(100)),2,2,1,'    h^{glac}',cax1);

% Plot GPR only model
F = scatteredInterpolant(xgpr,ygpr,Hgpr,'natural','none');
Hgpronly2 = F(xx,yy);
Hgpronly2(isnan(TMP.Z)) = NaN;
Hgpronly2(Hgpronly2 == 0) = NaN;
plot_results(DEM,Hgpronly2,dbx,dby,[],[],[],flipud(viridis(100)),2,2,2,'    h^{GPR}',cax1);
hold on;
plot(xgpr,ygpr,'k.','markersize',6);

% Plot GlaTE model and its difference to unconstrained model
plot_results(DEM,Hall,dbx,dby,[],[],[],flipud(viridis(100)),2,2,3,'    h^{est}',cax1);
plot_results(DEM,Hall-gfac*Hgla,dbx,dby,[],[],[],flipud(brewermap(100,'PuOr')),2,2,4,'h^{est} - h^{glac}',cax2);

function plot_results(DEM,inpm,dbx,dby,xgpr,ygpr,Hgpr,cm,sp1,sp2,sp3,tit,cax)
%

aa = DEM;
aa.Z = inpm;
marksize = 1;

spi = subplot(sp1,sp2,sp3);
imagesc(aa,'AlphaData',~isnan(aa.Z))
hold on
plot(dbx,dby,'k.','MarkerSize',marksize)
h = title(tit);
set(h,'Fontsize',9);
colormap(spi,cm)
h = colorbar;
hh = get(h,'title');
set(hh,'string','[m]');
caxis(cax)
if (isempty(xgpr) == 0)
    s = scatter(xgpr(~isnan(Hgpr)),ygpr(~isnan(Hgpr)),'k.');
    s.MarkerFaceAlpha = 0;
    s.MarkerEdgeAlpha = 0.25; %0.008;
end
marg_x = 0.1 * (max(dbx) - min(dbx));
marg_y = 0.1 * (max(dby) - min(dby));
axis([min(dbx)-marg_x max(dbx)+marg_x min(dby)-marg_y max(dby) + marg_y]);
set(gca,'dataaspectratio',[1 1 1]);
h = xlabel('E-W coordinate [m]');
p = get(h,'Position');
if (sp1 == 1), p(2) = p(2) - 100; end
if (sp1 >= 2), p(2) = p(2) - 200; end
set(h,'Position',p);
ylabel('N-S coordinate [m]');
