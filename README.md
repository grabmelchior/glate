# GlaTE

Glacier thickness modeling algorithm

**What is GlaTE?**

GlaTE stands for Glacier Thickness Estimation. Based on (i) a digital elevation
model (DEM), (ii) a glacier mask (glacierized areas within DEM), and (iii) 
ground truth data (e.g., from GPR measurements), the ice thickness of a 
glacierized area is computed. A more detailed description is provided
in Langhammer et al. (2019).

GlaTE was extended as described in Grab et al. (2021), to account for debris 
covered areas. Furthermore an additional boundary constraint was introduced 
to account for ice thickness gradients in the vicinity of the glacier boundary.

**How to install GlaTE?**

This merely requires downloading the *.m files in the matlab folder.
Additionally, it is required to install the topo-toolbox, which is 
available from  https://topotoolbox.wordpress.com/ .
Of course, Matlab needs to be installed as well, including the image
processing toolbox.

**How to run GlaTE?**

After having read the Langhammer et al (2019) paper, it is recommended
to start playing with the GlaTE wrapper provided in the matlab directory.
It demonstrates the usage of GlaTE. The data sets from the paper are provided 
in the Data directory, with which you can play within the GlaTE wrapper.

**How to cite GlaTE?**

Please reference the Langhammer et al. (2019) paper, which is provided
in the Documents folder. Furthermore, the authors of the TopoToolbox should 
be acknowledged. 

The study of Langhammer et al. (2019) can be found here: 
https://doi.org/10.5194/tc-13-2189-2019
The study of Grab et al. (2021) here: 
https://doi.org/10.1017/jog.2021.55


**Providing feedback**

When you employ GlaTE for your work, we would highly appreciate any feedback
concerning 
*  potential bugs,
*  methodological issues, and
*  modifications or extensions that you have made

Please send your feedback to hansruedi.maurer@erdw.ethz.ch

**Updates**

We plan to update continuously the GlaTE package. It might be worthwhile,
checking the repository from time to time for updates.

I hope that this software will be useful for your work.

Zurich, June 29, 2019

Hansruedi Maurer
Department of Earth Sciences ETH Zurich
Soneggstrasse 5
8092 Zurich
Switzerland
